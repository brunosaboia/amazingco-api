﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmazingCo.API.Data;
using AmazingCo.API.Data.DTO;
using AmazingCo.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AmazingCo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NodesController : ControllerBase
    {
        /// <summary>
        /// Gets the rdata repostory
        /// </summary>
        private readonly INodeRepository _repository;
        /// <summary>
        /// Gets the application logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instace of <see cref="NodesController" /> class
        /// </summary>
        /// <param name="repository">The repository used to manipulate data</param>
        /// <param name="loggerFactory">The logger factory</param>
        public NodesController(INodeRepository repository, ILoggerFactory loggerFactory)
        {
            _repository = repository;
            _logger = loggerFactory.CreateLogger("Nodes Controller");
        }

        /// <summary>
        /// Creates a new node
        /// </summary>
        /// <param name="parentId">The parent node's ID of the new node</param>
        /// <param name="identification">The identification of the new node</param>
        /// <returns>The newly created node</returns>
        [HttpPost]
        public async Task<IActionResult> Create(int parentId, string identification)
        {
            var parent = await _repository.GetNodeById(parentId);

            if (parent == null)
            {
                return BadRequest($"Cannot add node: parent with id {parentId} was not found");
            }

            var node = new Node { Identification = identification, Parent = parent, Height = parent.Height + 1 };

            _repository.AddNode(node);

            return Created(string.Empty, node);
        }

        /// <summary>
        /// Gets all nodes
        /// </summary>
        /// <returns>A collection of all nodes, packed in <see cref="NodeDto" /> class</returns>
        [HttpGet]
        public IEnumerable<NodeDto> GetNodes()
        {
            // We create the variable now to avoid uncessary DB travelling
            var rootNode = _repository.RootNode;

            return _repository.Nodes.Select(n => NodeDto.FromNodeAndRoot(n, rootNode));
        }

        /// <summary>
        /// Gets a single node by its ID
        /// </summary>
        /// <param name="id">The ID of the node</param>
        /// <returns>The desired node</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var node = await _repository.GetNodeById(id);
            if (node == null)
            {
                return NotFound();
            }
            //? Do we _really_ need to pass the root node in all calls?
            //? This seems to be redundant and almost irrelevant data
            var nodeDto = NodeDto.FromNodeAndRoot(node, _repository.RootNode);

            return Ok(nodeDto);
        }

        /// <summary>
        /// Gets all the children of a given node
        /// </summary>
        /// <param name="id">The ID of the node</param>
        /// <returns>
        /// A collection of all nodes that are children of the given node,
        /// packed in <see cref="NodeDto" /> class
        /// </returns>
        [HttpGet("children/{id}")]
        public async Task<IActionResult> GetChildren(int id)
        {
            var node = await _repository.GetNodeById(id);
            if (node == null)
            {
                return NotFound();
            }
            var childrenNodes = _repository.GetNodeChildren(node);

            return Ok(childrenNodes);
        }

        /// <summary>
        /// Gets all the descendents (i.e the children and the children of the children) of a given node
        /// </summary>
        /// <param name="id">The ID of the node</param>
        /// <returns>
        /// A collection of all nodes that are descendents of the given node,
        /// packed in <see cref="NodeDto" /> class
        /// </returns>
        [HttpGet("descendants/{id}")]
        public async Task<IActionResult> GetDescendants(int id)
        {
            var node = await _repository.GetNodeById(id);
            if (node == null)
            {
                return NotFound();
            }
            var descendantsNodes = _repository.GetNodeDescendents(node);

            return Ok(descendantsNodes);
        }

        /// <summary>
        /// Changes a node's parent
        /// </summary>
        /// <param name="childId">The ID of the node to change the parent</param>
        /// <param name="newParentId">The ID of the target new parent</param>
        [HttpPatch("change-parent/{childId}/{newParentId}")]
        public async Task<IActionResult> ChangeNodeParent(int childId, int newParentId)
        {
            try
            {
                var child = await _repository.GetNodeById(childId);
                if (child == null) return NotFound($"Child node with ID {childId} not found");

                var newParent = await _repository.GetNodeById(newParentId);
                if (newParent == null) return NotFound($"Parent node with ID {childId} not found");

                await _repository.ChangeNodeParent(child, newParent);
                return NoContent();
            }
            catch (InvalidOperationException iope)
            {
                return BadRequest(new { Error = iope.Message });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error changing node parent. Child ID: {0}, Parent ID: {1}", childId, newParentId);
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Removes a node and its children from the collection. Cannot remove the root node
        /// </summary>
        /// <param name="id">The ID of the node to be removed</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var node = await _repository.GetNodeById(id);
            if(node == null) return NotFound();

            if(_repository.IsRootNode(node))
            {
                return BadRequest(new { Error = "Cannot delete the master node" });
            }

            _repository.RemoveNode(id);

            return NoContent();
        }
    }
}
