using AmazingCo.API.Models;

namespace AmazingCo.API.Data.DTO
{
    /// <summary>
    /// Represents a Node DTO (Data Transfer Object)
    /// </summary>
    public class NodeDto
    {
        /// <summary>
        /// The node's ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The identification (label) of the node
        /// </summary>
        public string Identification { get; set; }
        /// <summary>
        /// The node's parent
        /// </summary>
        public NodeDto Parent { get; set; }
        /// <summary>
        /// The node's height
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// The tree's root node
        /// </summary>
        public NodeDto Root { get; set; }

        /// <summary>
        /// Recursively creates a <see cref="NodeDto" /> object from a node and a root node
        /// </summary>
        /// <param name="node">The node to create the DTO from</param>
        /// <param name="rootNode">The tree's root node</param>
        /// <returns></returns>
        public static NodeDto FromNodeAndRoot(Node node, Node rootNode)
        {
            if (node == null) return null;

            return new NodeDto
            {
                Identification = node.Identification,
                Id = node.Id,
                Parent = FromNodeAndRoot(node.Parent, rootNode),
                Height = node.Height,
                Root = FromNodeAndRoot(rootNode, null)
            };
        }
    }
}