using AmazingCo.API.Models;
using Microsoft.EntityFrameworkCore;

namespace AmazingCo.API.Data
{
    /// <summary>
    /// Represents the application DataContext, which interacts with the database
    /// </summary>
    public class DataContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of DataContext
        /// </summary>
        /// <param name="options">The database options</param>
        /// <returns>A new DataContext instance</returns>
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}

        /// <summary>
        /// Represents the nodes stored in the database
        /// </summary>
        public DbSet<Node> Nodes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Node>().HasData
            (
                new Node { Id = 1, Identification = "Root Node", Height = 0, Parent = null },
                new Node { Id = 2, Identification = "A", Height = 1, ParentId = 1 },
                new Node { Id = 3, Identification = "B", Height = 1, ParentId = 1 },
                new Node { Id = 4, Identification = "C", Height = 2, ParentId = 2 }
            );
        }
    }
}