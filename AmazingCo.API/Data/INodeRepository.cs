using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AmazingCo.API.Models;

namespace AmazingCo.API.Data
{
    /// <summary>
    /// Represents a node repository interface.
    /// The purpose of the interface is to provide methods to get and manipulate data
    /// </summary>
    public interface INodeRepository
    {
        /// <summary>
        /// Get all the nodes from the data provider
        /// </summary>
        /// <returns>A collection of all nodes</returns>
        IEnumerable<Node> Nodes { get; }

        /// <summary>
        /// Gets a node with a given ID
        /// </summary>
        /// <param name="id">The node ID</param>
        /// <returns>A node with the specified ID</returns>
        Task<Node> GetNodeById(int id);

        /// <summary>
        /// Gets a node's parent
        /// </summary>
        /// <param name="node">The node to get the parent from</param>
        /// <returns>The node parent</returns>
        Task<Node> GetNodeParent(Node node);

        /// <summary>
        /// Determines whether a node exists
        /// </summary>
        /// <param name="id">The ID of the target node</param>
        /// <returns>true if a node with a given ID exists, false otherwise</returns>
        Task<bool> NodeExists(int id);

        /// <summary>
        /// Returns whether the node is the root node
        /// </summary>
        /// <param name="node">The node to check if it is the root</param>
        /// <returns>true if a node is the root, false otherwise</returns>
        bool IsRootNode(Node node);

        /// <summary>
        /// Adds a node
        /// </summary>
        /// <param name="node">The node to be added</param>
        void AddNode(Node node);

        /// <summary>
        /// Removes a node
        /// </summary>
        /// <param name="node">The node to be removed</param>
        void RemoveNode(Node node);

        /// <summary>
        /// Removes a node from its ID
        /// </summary>
        /// <param name="node">The node ID to be removed</param>
        void RemoveNode(int id);

        /// <summary>
        /// Gets all the children of a given node
        /// </summary>
        /// <param name="node">The node to get children from</param>
        /// <returns>A collection of all the children of a given node</returns>
        IEnumerable<Node> GetNodeChildren(Node node);

        /// <summary>
        /// Get all the descendents of a given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        IEnumerable<Node> GetNodeDescendents(Node node);

        /// <summary>
        /// Changes a node's parent
        /// </summary>
        /// <param name="child">The node to assign a new parent to</param>
        /// <param name="newParent">The new parent of the node</param>
        Task ChangeNodeParent(Node child, Node newParent);

        /// <summary>
        /// Gets the root node
        /// </summary>
        Node RootNode { get; }
    }
}