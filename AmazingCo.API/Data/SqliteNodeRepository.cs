using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AmazingCo.API.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace AmazingCo.API.Data
{
    /// <summary>
    /// Implementation of INodeRepository that uses a DataContext to fetch data from a Sqlite database
    /// </summary>
    public class SqliteNodeRepository : INodeRepository
    {
        /// <summary>
        /// Initializes a new instance of SqlNodeRepository
        /// </summary>
        /// <param name="context">The DataContext used to fetch database data</param>
        public SqliteNodeRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// The DataContext which providers DB data
        /// </summary>
        private readonly DataContext _context;

        /// <summary>
        /// Adds a node to the database
        /// </summary>
        /// <param name="node">The node to be added</param>
        /// <returns></returns>
        public async void AddNode(Node node)
        {
            _context.Nodes.Add(node);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets a node by its ID
        /// </summary>
        /// <param name="id">The node ID</param>
        /// <returns>A node with the specified ID, or null if no node found</returns>
        public async Task<Node> GetNodeById(int id)
        {
            var node = await _context.Nodes
                .Include(n => n.Parent)
                .FirstOrDefaultAsync(n => n.Id == id);

            return node;
        }

        /// <summary>
        /// Returns all the children of a specific node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public IEnumerable<Node> GetNodeChildren(Node node)
        {
            var nodes = _context.Nodes.Where(n => n.ParentId == node.Id);

            return nodes;
        }

        /// <summary>
        /// Gets the parent of a specific node
        /// </summary>
        /// <param name="node">Gets the parent node of a given node</param>
        /// <returns>The parent node of </returns>
        public async Task<Node> GetNodeParent(Node node)
        {
            var parentNode = await
                _context.Nodes.SingleOrDefaultAsync(n => n.Id == node.Parent.Id);

            return parentNode;
        }

        /// <summary>
        /// Get all the nodes saved on the database
        ///
        /// ! (Caution: this method can be heavy if there are several nodes on the database)
        /// </summary>
        public IEnumerable<Node> Nodes => _context.Nodes.OrderBy(n => n.Height).ToList();

        /// <summary>
        /// Gets the root node
        /// </summary>
        public Node RootNode => _context.Nodes.SingleOrDefault(n => n.ParentId == null);


        /// <summary>
        /// Determines if a node is the root node
        /// </summary>
        /// <param name="node">The node to check if its the root</param>
        /// <returns>true if the node is the root node, false otherwise</returns>
        public bool IsRootNode(Node node)
        {
            return node.Height == 0 && node.ParentId == null;
        }

        /// <summary>
        /// Checks whether a node exists
        /// </summary>
        /// <param name="id">The id of the node to check if exists</param>
        /// <returns></returns>
        public async Task<bool> NodeExists(int id)
        {
            var nodeExists = await _context.Nodes.AnyAsync(n => n.Id == id);

            return nodeExists;
        }

        /// <summary>
        /// Removes a node from the database
        /// </summary>
        /// <param name="node">The node to remove</param>
        /// <returns></returns>
        public async void RemoveNode(Node node)
        {
            _context.Nodes.Remove(node);

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Removes a node from the database
        /// </summary>
        /// <param name="id">The ID of the node to remove</param>
        /// <returns></returns>
        public async void RemoveNode(int id)
        {
            var node = await this.GetNodeById(id);

            if (node != null)
            {
                this.RemoveNode(node);
            }
        }

        /// <summary>
        /// Find all the descendants of a given node
        /// </summary>
        /// <param name="id">The ID of the node to find all the descendants</param>
        /// <param name="includeSelf">Whether you should include the </param>
        /// <returns>A collection of all the descendants of a given node</returns>
        private IQueryable<Node> _FindAllDescendants(int id, bool includeSelf = false)
        {
            var user = new SqliteParameter("id", id);

            // The following CTE and query gets all the descendent nodes of the tree recursively
            // This is a good way to avoid uncessary round trips to the database
            // The disadvantage is that we need to re-implement this query if we change the underlying
            // database (i.e this is not ANSI SQL -- it is SQLite specific).
            // But since we have dependency injection, we can have environment specific repositories
            var sql = $@"WITH RECURSIVE get_all_descendants(i)
  AS(
        SELECT Id FROM Nodes
        WHERE id = @id
        UNION
        SELECT Id FROM Nodes, get_all_descendants
        WHERE Nodes.ParentId = get_all_descendants.i
    )
SELECT * FROM Nodes
WHERE Id IN get_all_descendants {(!includeSelf ? "AND Id != @id" : string.Empty)};";

            var nodes = _context.Nodes.FromSql(sql, user);

            return nodes;
        }

        /// <summary>
        /// Changes a target's parent node
        /// </summary>
        /// <param name="child"></param>
        /// <param name="newParent"></param>
        /// <returns></returns>
        public async Task ChangeNodeParent(Node child, Node newParent)
        {
            if (IsRootNode(child))
            {
                throw new InvalidOperationException("Cannot move the root node");
            }
            if (child.ParentId == newParent.Id) return;
            if (child.Id == newParent.Id)
            {
                throw new InvalidOperationException("Node cannot be parent of itself");
            }

            var childDescendents = _FindAllDescendants(child.Id).OrderBy(n => n.Height);

            if (childDescendents.Any(n => n.Id == newParent.Id))
            {
                // We cannot move a parent node to a child one without know what to do with the possible
                // orphans that would arise, or with data duplication
                throw new InvalidOperationException("Cannot set a node's parenthood to one of its child");
            }

            var originalChildHeight = child.Height;
            var newChildHeight = newParent.Height + 1;

            child.Height = newChildHeight;
            child.Parent = newParent;

            // If the original height is different from the new height, then
            // we need to update the height of all descendents, if any
            if (newChildHeight != originalChildHeight && childDescendents.Count() > 0)
            {
                var heightOffSet = newChildHeight - originalChildHeight;

                foreach (var node in childDescendents)
                {
                    node.Height += heightOffSet;
                }
            }

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets all the nodes which descend from another node
        /// </summary>
        /// <param name="node">The root to get descendents</param>
        /// <returns>A collection of the descendent nodes</returns>
        public IEnumerable<Node> GetNodeDescendents(Node node)
        {
            return _FindAllDescendants(node.Id);
        }
    }
}