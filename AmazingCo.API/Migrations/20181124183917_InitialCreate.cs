﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AmazingCo.API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Nodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Identification = table.Column<string>(nullable: true),
                    Height = table.Column<int>(nullable: false),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Nodes_Nodes_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Nodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Nodes",
                columns: new[] { "Id", "Height", "Identification", "ParentId" },
                values: new object[] { 1, 0, "Root Node", null });

            migrationBuilder.InsertData(
                table: "Nodes",
                columns: new[] { "Id", "Height", "Identification", "ParentId" },
                values: new object[] { 2, 1, "A", 1 });

            migrationBuilder.InsertData(
                table: "Nodes",
                columns: new[] { "Id", "Height", "Identification", "ParentId" },
                values: new object[] { 3, 1, "B", 1 });

            migrationBuilder.InsertData(
                table: "Nodes",
                columns: new[] { "Id", "Height", "Identification", "ParentId" },
                values: new object[] { 4, 2, "C", 2 });

            migrationBuilder.CreateIndex(
                name: "IX_Nodes_ParentId",
                table: "Nodes",
                column: "ParentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Nodes");
        }
    }
}
