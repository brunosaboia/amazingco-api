using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AmazingCo.API.Models
{
    /// <summary>
    /// Represents a node model
    /// </summary>
    public class Node
    {
        /// <summary>
        /// Gets or sets the ID of the node
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the parent of the node. If this value is null, then the node is root
        /// </summary>
        public virtual Node Parent { get; set; }
        /// <summary>
        /// Gets or sets the data associated with the node
        /// </summary>
        public string Identification { get; set; }
        /// <summary>
        /// Gets or sets the height of the node relative to the root node
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Gets or sets the ID of the parent node
        /// </summary>
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
    }
}