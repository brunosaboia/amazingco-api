# Amazing CO Tree API

Bruno's implementation of the Amazing CO Tree API Challenge. See the [challenge description](CHALLENGE.md) for more information.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need [Docker](https://docker.com) installed to run it as a container.

Otherwise, you need [.NET Core SDK 2.2](https://www.microsoft.com/net/download/dotnet-core/2.2).


### Installing

If you are using Docker, go to `AmazingCO.API` directory and build the image:

```
cd AmazingCo.API && docker build -t amazingco-api .
```

Then start the container:
```
docker run -d -p 8080:80 --name amazingco-app amazingco-api
```

To test if the env is working, try to fetch some data:

```
curl localhost:8080/api/nodes/
```

### Endpoints

Endpoint docs are coming soon

## Running the tests

Tests are coming soon

## Author

* **Bruno Saboia de Albuquerque** - *Main Developer* - [brunosaboia](https://github.com/brunosaboia)

## License

This project is licensed under the MIT License.

